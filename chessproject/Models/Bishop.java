package chessproject.Models;

import javafx.scene.image.ImageView;
import chess.FXMLDocumentController;
import static chess.FXMLDocumentController.moves;
import static chess.FXMLDocumentController.turn;
import javafx.scene.image.Image;

public class Bishop extends Piece {

    public Bishop(int i, int j, Player owner, ImageView image, House house) {
        super(i, j, owner, image, house);
        this.index = 2;
    }

    @Override
    public void Move(House origin, House destination) {
        if (Math.abs(destination.i - origin.i) == Math.abs(destination.j - origin.j) && destination.piece == null && !origin.piece.isAchsen) {

            if (destination.i > origin.i && destination.j > origin.j) {
                int k = 9; //checking next houses without changing origin i or j
                boolean isempty = true;
                while (origin.index + k <= destination.index) {
                    if (FXMLDocumentController.houses[origin.index + k].piece == null) {
                        k += 9;
                    } else {
                        isempty = false;
                        return;
                    }
                }
                if (isempty) {
                    Image image = new Image(FXMLDocumentController.class.getResourceAsStream(FXMLDocumentController.images[origin.piece.index][FXMLDocumentController.turn % 2][destination.color]));
                    destination.imageView.setImage(image);
                    origin.imageView.setImage(null);
                }
            }
            if (destination.i < origin.i && destination.j < origin.j) {
                int k = 9;
                boolean isempty = true;
                while (origin.index - k >= destination.index) {
                    if (FXMLDocumentController.houses[origin.index - k].piece == null) {
                        k += 9;
                    } else {
                        isempty = false;
                        return;
                    }
                }
                if (isempty) {
                    Image image = new Image(FXMLDocumentController.class.getResourceAsStream(FXMLDocumentController.images[origin.piece.index][FXMLDocumentController.turn % 2][destination.color]));
                    destination.imageView.setImage(image);
                    origin.imageView.setImage(null);
                }
            }
            if (destination.i > origin.i && destination.j < origin.j) {
                int k = 7;
                boolean isempty = true;
                while (origin.index + k <= destination.index) {
                    if (FXMLDocumentController.houses[origin.index + k].piece == null) {
                        k += 7;
                    } else {
                        isempty = false;
                        return;
                    }
                }
                if (isempty) {
                    Image image = new Image(FXMLDocumentController.class.getResourceAsStream(FXMLDocumentController.images[origin.piece.index][FXMLDocumentController.turn % 2][destination.color]));
                    destination.imageView.setImage(image);
                    origin.imageView.setImage(null);
                }
            }
            if (destination.i < origin.i && destination.j > origin.j) {
                int k = 7;
                boolean isempty = true;
                while (origin.index - k >= destination.index) {
                    if (FXMLDocumentController.houses[origin.index - k].piece == null) {
                        k += 7;
                    } else {
                        isempty = false;
                        return;
                    }
                }
                if (isempty) {
                    Image image = new Image(FXMLDocumentController.class.getResourceAsStream(FXMLDocumentController.images[origin.piece.index][FXMLDocumentController.turn % 2][destination.color]));
                    destination.imageView.setImage(image);
                    origin.imageView.setImage(null);
                }
            }
            FXMLDocumentController.players[turn % 2].ChosenPiece = null;
            this.i = destination.i;
            this.j = destination.j;
            this.isChosen = false;Move move = new Move(origin, destination,destination.piece);moves.add(move);
            this.house = destination;
            destination.piece = origin.piece;
            origin.piece = null;
            turn++;
            
        }
        else if (Math.abs(destination.i - origin.i) == Math.abs(destination.j - origin.j)
                && destination.piece != null && destination.piece.owner != FXMLDocumentController.players[turn%2]&&!origin.piece.isAchsen) {

            if (destination.i > origin.i && destination.j > origin.j) {
                int k = 9; //checking next houses without changing origin i or j
                boolean isempty = true;
                while (origin.index + k < destination.index) {
                    if (FXMLDocumentController.houses[origin.index + k].piece == null) {
                        k += 9;
                    } else {
                        isempty = false;
                        return;
                    }
                }
                if (isempty) {
                    Image image = new Image(FXMLDocumentController.class.getResourceAsStream(FXMLDocumentController.images[origin.piece.index][FXMLDocumentController.turn % 2][destination.color]));
                    destination.imageView.setImage(image);
                    origin.imageView.setImage(null);
                    if (FXMLDocumentController.turn % 2 == 1) {
                        if (destination.piece != null) {
                            FXMLDocumentController.player1_pieces.remove(destination.piece);
                        }
                    } else {
                        if (destination.piece != null) {
                            FXMLDocumentController.player2_pieces.remove(destination.piece);
                        }
                    }
                }
            }
            if (destination.i < origin.i && destination.j < origin.j) {
                int k = 9;
                boolean isempty = true;
                while (origin.index - k > destination.index) {
                    if (FXMLDocumentController.houses[origin.index - k].piece == null) {
                        k += 9;
                    } else {
                        isempty = false;
                        return;
                    }
                }
                if (isempty) {
                    Image image = new Image(FXMLDocumentController.class.getResourceAsStream(FXMLDocumentController.images[origin.piece.index][FXMLDocumentController.turn % 2][destination.color]));
                    destination.imageView.setImage(image);
                    origin.imageView.setImage(null);
                    if (FXMLDocumentController.turn % 2 == 1) {
                        if (destination.piece != null) {
                            FXMLDocumentController.player1_pieces.remove(destination.piece);
                        }
                    } else {
                        if (destination.piece != null) {
                            FXMLDocumentController.player2_pieces.remove(destination.piece);
                        }
                    }
                }
            }
            if (destination.i > origin.i && destination.j < origin.j) {
                int k = 7;
                boolean isempty = true;
                while (origin.index + k < destination.index) {
                    if (FXMLDocumentController.houses[origin.index + k].piece == null) {
                        k += 7;
                    } else {
                        isempty = false;
                        return;
                    }
                }
                if (isempty) {
                    Image image = new Image(FXMLDocumentController.class.getResourceAsStream(FXMLDocumentController.images[origin.piece.index][FXMLDocumentController.turn % 2][destination.color]));
                    destination.imageView.setImage(image);
                    origin.imageView.setImage(null);
                    if (FXMLDocumentController.turn % 2 == 1) {
                        if (destination.piece != null) {
                            FXMLDocumentController.player1_pieces.remove(destination.piece);
                        }
                    } else {
                        if (destination.piece != null) {
                            FXMLDocumentController.player2_pieces.remove(destination.piece);
                        }
                    }
                }
            }
            if (destination.i < origin.i && destination.j > origin.j) {
                int k = 7;
                boolean isempty = true;
                while (origin.index - k > destination.index) {
                    if (FXMLDocumentController.houses[origin.index - k].piece == null) {
                        k += 7;
                    } else {
                        isempty = false;
                        return;
                    }
                }
                if (isempty) {
                    Image image = new Image(FXMLDocumentController.class.getResourceAsStream(FXMLDocumentController.images[origin.piece.index][FXMLDocumentController.turn % 2][destination.color]));
                    destination.imageView.setImage(image);
                    origin.imageView.setImage(null);
                    if (FXMLDocumentController.turn % 2 == 1) {
                        if (destination.piece != null) {
                            FXMLDocumentController.player1_pieces.remove(destination.piece);
                        }
                    } else {
                        if (destination.piece != null) {
                            FXMLDocumentController.player2_pieces.remove(destination.piece);
                        }
                    }
                }
            }
            FXMLDocumentController.players[turn % 2].ChosenPiece = null;
            this.i = destination.i;
            this.j = destination.j;
            this.isChosen = false;
            Move move = new Move(origin, destination,destination.piece);moves.add(move);
            this.house = destination;
            destination.piece = origin.piece;
            origin.piece = null;
            turn++;
            
        }
    }

}
