package chessproject.Models;

import chess.FXMLDocumentController;
import static chess.FXMLDocumentController.moves;
import static chess.FXMLDocumentController.turn;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class King extends Piece {

    public boolean hasChecked = false;

    public King(int i, int j, Player owner, ImageView image, House house) {
        super(i, j, owner, image, house);
        this.index = 5;
    }

    @Override
    public void Move(House origin, House destination) {
        if (destination.piece == null && ((Math.abs(destination.i - origin.i) == 1 && Math.abs(destination.j - origin.j) == 0)||(Math.abs(destination.i - origin.i) == 0 && Math.abs(destination.j - origin.j) == 1)||
                (Math.abs(destination.i - origin.i) == 1 && Math.abs(destination.j - origin.j) == 1))) {

            Image image = new Image(FXMLDocumentController.class.getResourceAsStream(FXMLDocumentController.images[origin.piece.index][turn % 2][destination.color]));
            destination.imageView.setImage(image);
            origin.imageView.setImage(null);
            FXMLDocumentController.players[turn % 2].ChosenPiece = null;
            this.i = destination.i;
            this.j = destination.j;
            this.isChosen = false;
            Move move = new Move(origin, destination,destination.piece);moves.add(move);
            this.house = destination;
            destination.piece = origin.piece;
            origin.piece = null;
            turn++;
            
//        } else if (destination.piece == null && (Math.abs(destination.i - origin.i) == 2 && Math.abs(destination.j - origin.j) == 1)) {
//
//            Image image = new Image(FXMLDocumentController.class.getResourceAsStream(FXMLDocumentController.images[origin.piece.index][FXMLDocumentController.turn % 2][destination.color]));
//            destination.imageView.setImage(image);
//            origin.imageView.setImage(null);
//
//            FXMLDocumentController.players[turn % 2].ChosenPiece = null;
//            this.i = destination.i;
//            this.j = destination.j;
//            this.isChosen = false;
//            this.house = destination;
//            destination.piece = origin.piece;
//            origin.piece = null;
//            turn++;
        } else if ((destination.piece != null && destination.piece.owner != FXMLDocumentController.players[turn%2]&&((Math.abs(destination.i - origin.i) == 1 && Math.abs(destination.j - origin.j) == 0)||(Math.abs(destination.i - origin.i) == 0 && Math.abs(destination.j - origin.j) == 1)
                ||(Math.abs(destination.i - origin.i) == 1 && Math.abs(destination.j - origin.j) == 1)))) {
            Image image = new Image(FXMLDocumentController.class.getResourceAsStream(FXMLDocumentController.images[origin.piece.index][FXMLDocumentController.turn % 2][destination.color]));
            Move move = new Move(origin, destination,destination.piece);moves.add(move);
            destination.imageView.setImage(image);
            origin.imageView.setImage(null);
            destination.piece = origin.piece;
            FXMLDocumentController.player1_pieces.remove(destination.piece);
            turn++;
            
        } else {
            return;
        }

    }

}
